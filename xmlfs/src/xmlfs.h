#ifndef XMLFS_H
# define XMLFS_H

struct xml_node {
  struct xml_node *nodes;
  char            *name;
  char            *value;
};

struct xml_private_data {
  int             uid;
  int             gid;
  struct vnode    *root_vp;
  struct xml_node *root_xml;
};

# define get_xml_private(mp) ((struct xml_private_data*)((mp)->mnt_data))

# define XML_ROOTINO  (2)

# define XMLNULL ((void *)0)
# define XMLFSTAG "[xmlfs] "
# define xmlfs_debug(format, args...) printf(XMLFSTAG "%s: " format, __func__, ## args)
# define xmlfs_trace() printf(XMLFSTAG "Trace: %s\n", __FUNCTION__)

extern struct vop_vector xmlfs_vnodeops;
extern struct xml_private_data g_info;

/**
 * Simple way to bypass diff between userland and kerneland malloc
*/
void *xmlfs_malloc(unsigned long size, void *type, int flags);

/**
 * xml part
*/
struct xml_node *xml_get_root(char *name);

#endif /* !XMLFS_H */
