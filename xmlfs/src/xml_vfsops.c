#include "xmlfs.h"

#include <sys/param.h>
#include <sys/systm.h>
#include <sys/proc.h>
#include <sys/bio.h>
#include <sys/buf.h>
#include <sys/kernel.h>
#include <sys/sysctl.h>
#include <sys/vnode.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/malloc.h>
#include <sys/module.h>

static const char       *xmlfs_opts[] = { "uid", "gid", "from", NULL };
struct xml_private_data g_info;

static MALLOC_DEFINE(M_XMLFSNODE, "xmlvfs_node", "KOKAFS vnode private part");

static int xmlvfs_cmount(struct mntarg *ma, void *data, uint64_t flags) {
  xmlfs_trace();
  return 0;
}

static int xmlvfs_mount(struct mount *mp) {
  int               error;
  struct vfsoptlist *opts;
  char              *path;

  opts = mp->mnt_optnew;

  xmlfs_debug("mp:%p\n", (void *)mp);
  if (vfs_filteropt(opts, xmlfs_opts))
    return EINVAL;

  vfs_getopt(opts, "fspath", (void **)&path, &error);
  xmlfs_debug("fspath: %s\n", path);

  vfs_getopt(opts, "uid", (void **)&g_info.uid, &error);
  xmlfs_debug("uid: %d err: %d\n", g_info.uid, error);
  vfs_getopt(opts, "gid", (void **)&g_info.gid, &error);
  xmlfs_debug("gid: %d err: %d\n", g_info.gid, error);

  vfs_getopt(opts, "from", (void **)&path, &error);
  xmlfs_debug("from: %s\n", path);
  xmlfs_debug("ref:%d\n", mp->mnt_ref);
  return 0;
}

static int xmlvfs_unmount(struct mount *mp, int mntflags) {
  int error;
  int flags;

  xmlfs_trace();

  if (mntflags & MNT_FORCE)
    flags = FORCECLOSE;

  error = vflush(mp, 0, flags, curthread);

  xmlfs_debug("ref:%d\n", mp->mnt_ref);

  return 0;
}

static int xmlvfs_root(struct mount *mt, int flags, struct vnode **vpp) {
  struct vnode *vp;
  int          error;

  xmlfs_trace();
  error = VFS_VGET(mt, XML_ROOTINO, LK_EXCLUSIVE, &vp);
  if (error)
    return error;

  xmlfs_debug("type: %d\n", vp->v_type);
  *vpp = vp;
  return 0;
}

static int xmlvfs_statfs(struct mount *mt, struct statfs *sbp) {
  xmlfs_trace();
  return 0;
}


int xmlvnops_init(struct mount *mt, struct vop_vector *fifoops, struct vnode **vpp);
static int xmlvfs_vget(struct mount *mp, ino_t ino, int flags, struct vnode **vpp) {
  int error;
  struct thread *td;
  struct vnode *vp;

  xmlfs_trace();
  td = curthread;
  error = vfs_hash_get(mp, ino, flags, td, vpp, NULL, NULL);
  if (error || *vpp != NULL) {
    xmlfs_debug("return after vfs_hash_get() error:%d vpp:%p\n", error, vpp);
    return error;
  }

  if ((error = getnewvnode("xmlfs", mp, &xmlfs_vnodeops, &vp)) != 0) {
    *vpp = NULL;
    xmlfs_debug("getnewvnode() = error (%d)\n", error);
    return (error);
  }

  error = insmntque(vp, mp);
  if (error != 0) {
    *vpp = NULL;
    xmlfs_debug("insmntque() = error (%d)\n", error);
    return (error);
  }
  error = vfs_hash_insert(vp, ino, flags, td, vpp, NULL, NULL);
  if (error || *vpp != NULL) {
    xmlfs_debug("vfs_hash_insert() = error (%d)\n", error);
    return (error);
  }

  xmlvnops_init(mp, NULL, &vp);

  *vpp = vp;
  return error;
}

static struct vfsops xmlvfs_vfsops = {
  .vfs_cmount   = xmlvfs_cmount,
  .vfs_mount    = xmlvfs_mount,
  .vfs_unmount  = xmlvfs_unmount,
  .vfs_root     = xmlvfs_root,
  .vfs_statfs   = xmlvfs_statfs,
  .vfs_vget     = xmlvfs_vget,
  /* Fill useless function by std* */
  .vfs_sync     = vfs_stdsync,
  .vfs_quotactl = vfs_stdquotactl,
  .vfs_init     = vfs_stdinit,
  .vfs_uninit   = vfs_stduninit
};

/**
 * See man page to understood this call
*/
VFS_SET(xmlvfs_vfsops, xmlfs, VFCF_NETWORK);
