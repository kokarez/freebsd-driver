#include "xmlfs.h"

#ifndef CUNIT_TEST
# include <sys/param.h>
# include <sys/systm.h>
# include <sys/kernel.h>
# include <sys/malloc.h>
#else
# define M_XMLNODE (NULL)
# define M_WAITOK  0
# define MALLOC_DEFINE(a,b,c) void *a = 0
#endif

static MALLOC_DEFINE(M_XMLNODE, "xml_node", "XML node private fs");

struct xml_node *xml_get_root(char *name)
{
  struct xml_node *root = XMLNULL;

  root = xmlfs_malloc(sizeof (struct xml_node), M_XMLNODE, M_WAITOK);
  return root;
}
