#include "xmlfs.h"

#include <sys/param.h>
#include <sys/systm.h>
#include <sys/namei.h>
#include <sys/kernel.h>
#include <sys/proc.h>
#include <sys/bio.h>
#include <sys/buf.h>
#include <sys/fcntl.h>
#include <sys/mount.h>
#include <sys/unistd.h>
#include <sys/vnode.h>
#include <sys/limits.h>
#include <sys/lockf.h>
#include <sys/stat.h>
#include <sys/dirent.h>

int xmlvnops_init(struct mount *mt, struct vop_vector *fifoops, struct vnode **vpp);
int xmlvnops_init(struct mount *mt, struct vop_vector *fifoops, struct vnode **vpp) {
  struct vnode *vp;

  xmlfs_trace();
  vp = *vpp;
  vp->v_type = VDIR;
  return 0;
}

static int xmlvnops_access(struct vop_access_args *ap)
{
  xmlfs_trace();
  /*
   * EROFS in case of readonly file and write access ask
   * EPERM in case of non permission
   */
  return 0;
}

static int xmlvnops_open(struct vop_open_args *ap)
{
  xmlfs_trace();

  vnode_create_vobject(ap->a_vp, 200, ap->a_td);

  return 0;
}

static int xmlvnops_close(struct vop_close_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_getattr(struct vop_getattr_args *ap)
{
  struct vattr *vap = ap->a_vap;
  xmlfs_trace();

  vap->va_uid = g_info.uid;
  vap->va_gid = g_info.gid;
  vap->va_fsid = 1664;
  vap->va_size = 6969;
  vap->va_type = VDIR;
  return 0;
}

static int xmlvnops_setattr(struct vop_setattr_args *ap)
{
  xmlfs_trace();
  return 0;
}

static int xmlvnops_read(struct vop_read_args *ap)
{
  xmlfs_trace();
  return 0;
}

static int xmlvnops_write(struct vop_write_args *ap)
{
  xmlfs_trace();
  return 0;
}

static int xmlvnops_create(struct vop_create_args *ap)
{
  xmlfs_trace();
  return 0;
}

static int xmlvnops_remove(struct vop_remove_args *ap)
{
  xmlfs_trace();
  return 0;
}

static int xmlvnops_rename(struct vop_rename_args *ap)
{
  xmlfs_trace();
  return 0;
}

static int xmlvnops_link(struct vop_link_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_symlink(struct vop_symlink_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_mknod(struct vop_mknod_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_mkdir(struct vop_mkdir_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_rmdir(struct vop_rmdir_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_readdir(struct vop_readdir_args *ap)
{
  struct uio    *uio;
  struct dirent dp;
  int           error = 0;

  xmlfs_trace();

  if (ap->a_vp->v_type != VDIR)
    return ENOTDIR;

  uio = ap->a_uio;
  if (uio->uio_offset < 0)
    return EINVAL;

  dp.d_fileno = 6969;
  dp.d_type = VREG;
  dp.d_namlen = 12;
  bcopy("Helloworld", dp.d_name, dp.d_namlen);
  dp.d_reclen = GENERIC_DIRSIZ(&dp);

  vfs_read_dirent(ap, &dp, 0);
  uio->uio_offset = dp.d_reclen;
  return error;
}

static int xmlvnops_fsync(struct vop_fsync_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_print (struct vop_print_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

/*
 *  see ext2 vnode implementation
 */
static int xmlvnops_pathconf (struct vop_pathconf_args *ap)
{
  xmlfs_trace();

	switch (ap->a_name) {
	case _PC_LINK_MAX:
		*ap->a_retval = 32000;
		return (0);
	case _PC_NAME_MAX:
		*ap->a_retval = NAME_MAX;
		return (0);
	case _PC_PATH_MAX:
		*ap->a_retval = PATH_MAX;
		return (0);
	case _PC_PIPE_BUF:
		*ap->a_retval = PIPE_BUF;
		return (0);
	case _PC_CHOWN_RESTRICTED:
		*ap->a_retval = 1;
		return (0);
	case _PC_NO_TRUNC:
		*ap->a_retval = 1;
		return (0);
	case _PC_MIN_HOLE_SIZE:
		return(EOPNOTSUPP);
	default:
		return (EINVAL);
	}

  /* unreachable code */
  return EOPNOTSUPP;
}

static int xmlvnops_strategy (struct vop_strategy_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_ioctl(struct vop_ioctl_args *ap)
{
  xmlfs_trace();
  return ENOTTY;
}

static int xmlvnops_getextattr(struct vop_getextattr_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_advlock(struct vop_advlock_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_lookup(struct vop_lookup_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_inactive(struct vop_inactive_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_reclaim(struct vop_reclaim_args *ap)
{
  xmlfs_trace();
  /*
  ** Don't retourne EOPNOTSUPP, else the kernel panic
  */
  return 0;
}

static int xmlvnops_getpages(struct vop_getpages_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

static int xmlvnops_putpages(struct vop_putpages_args *ap)
{
  xmlfs_trace();
  return EOPNOTSUPP;
}

struct vop_vector xmlfs_vnodeops = {
    .vop_default    =   &default_vnodeops,
    .vop_access     =   xmlvnops_access,
    .vop_advlock    =   xmlvnops_advlock,
    .vop_close      =   xmlvnops_close,
    .vop_create     =   xmlvnops_create,
    .vop_fsync      =   xmlvnops_fsync,
    .vop_getattr    =   xmlvnops_getattr,
    .vop_getextattr =   xmlvnops_getextattr,
    .vop_getpages   =   xmlvnops_getpages,
    .vop_inactive   =   xmlvnops_inactive,
    .vop_ioctl      =   xmlvnops_ioctl,
    .vop_link       =   xmlvnops_link,
    .vop_lookup     =   xmlvnops_lookup,
    .vop_mkdir      =   xmlvnops_mkdir,
    .vop_mknod      =   xmlvnops_mknod,
    .vop_open       =   xmlvnops_open,
    .vop_pathconf   =   xmlvnops_pathconf,
    .vop_print      =   xmlvnops_print,
    .vop_putpages   =   xmlvnops_putpages,
    .vop_read       =   xmlvnops_read,
    .vop_readdir    =   xmlvnops_readdir,
    .vop_reclaim    =   xmlvnops_reclaim,
    .vop_remove     =   xmlvnops_remove,
    .vop_rename     =   xmlvnops_rename,
    .vop_rmdir      =   xmlvnops_rmdir,
    .vop_setattr    =   xmlvnops_setattr,
    .vop_strategy   =   xmlvnops_strategy,
    .vop_symlink    =   xmlvnops_symlink,
    .vop_write      =   xmlvnops_write,
};
