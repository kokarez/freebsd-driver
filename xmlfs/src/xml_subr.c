#include "xmlfs.h"

#include <sys/param.h>
#include <sys/systm.h>
#include <sys/malloc.h>

/**
 * The only goal of this function is to overwride malloc function.
 * Necessary to compile in userland for the unit test.
*/
void *xmlfs_malloc(unsigned long size, void *type, int flags)
{
  return malloc(size, type, flags);
}
