#include <sys/types.h>

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

int open_dir(const char *file) {
  DIR           *dir;
  struct dirent *dp;

  dir = opendir(file);
  if (!dir) {
    printf("%s: failed to open %s: %s\n", __func__, file, strerror(errno));
    return -EINVAL;
  }
  dp = readdir(dir);
  printf("name: %.*s\n", dp->d_namlen, dp->d_name);
  dp = readdir(dir);
  printf("name: %.*s\n", dp->d_namlen, dp->d_name);
  closedir(dir);
  return 0;
}

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Usage: %s /mount/point\n", *argv);
    return -EINVAL;
  }
  open_dir(argv[1]);
  return 0;
}
