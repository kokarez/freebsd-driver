#include <stdlib.h>
#include <cunit/cunit.h>
#include <stdlib.h>

#include "../../xmlfs.h"

int main(void) {
  return 0;
}

void *xmlfs_malloc(unsigned long size,
                   void *type __attribute__((unused)),
                   int flags __attribute__((unused))) {
  return malloc(size);
}

void test_simple_xml(void) {
  struct xml_node *root = NULL;

  root = xml_get_root("../assets/simple.xml");

  cu_assert(root != NULL);

} CUNIT_FUNC(test_simple_xml, CUNIT_TYPE_TEST);
