#include "kokafs.h"

#include <sys/param.h>
#include <sys/systm.h>
#include <sys/namei.h>
#include <sys/kernel.h>
#include <sys/proc.h>
#include <sys/bio.h>
#include <sys/buf.h>
#include <sys/fcntl.h>
#include <sys/mount.h>
#include <sys/unistd.h>
#include <sys/vnode.h>
#include <sys/limits.h>
#include <sys/lockf.h>
#include <sys/stat.h>

static int kokavfs_access(struct vop_access_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_open(struct vop_open_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_close(struct vop_close_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_getattr(struct vop_getattr_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_setattr(struct vop_setattr_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_read(struct vop_read_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_write(struct vop_write_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_create(struct vop_create_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_remove(struct vop_remove_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_rename(struct vop_rename_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_link(struct vop_link_args *ap)
{
  kokafs_trace();
  return EOPNOTSUPP;
}

static int kokavfs_symlink(struct vop_symlink_args *ap)
{
  kokafs_trace();
  return EOPNOTSUPP;
}

static int kokavfs_mknod(struct vop_mknod_args *ap)
{
  kokafs_trace();
  return EOPNOTSUPP;
}

static int kokavfs_mkdir(struct vop_mkdir_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_rmdir(struct vop_rmdir_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_readdir(struct vop_readdir_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_fsync(struct vop_fsync_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_print (struct vop_print_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_pathconf (struct vop_pathconf_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_strategy (struct vop_strategy_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_ioctl(struct vop_ioctl_args *ap)
{
  kokafs_trace();
  return ENOTTY;
}

static int kokavfs_getextattr(struct vop_getextattr_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_advlock(struct vop_advlock_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_lookup(struct vop_lookup_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_inactive(struct vop_inactive_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_reclaim(struct vop_reclaim_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_getpages(struct vop_getpages_args *ap)
{
  kokafs_trace();
  return 0;
}

static int kokavfs_putpages(struct vop_putpages_args *ap)
{
  kokafs_trace();
  return 0;
}

struct vop_vector kokafs_vnodeops = {
    .vop_default    =   &default_vnodeops,
    .vop_access     =   kokavfs_access,
    .vop_advlock    =   kokavfs_advlock,
    .vop_close      =   kokavfs_close,
    .vop_create     =   kokavfs_create,
    .vop_fsync      =   kokavfs_fsync,
    .vop_getattr    =   kokavfs_getattr,
    .vop_getextattr =   kokavfs_getextattr,
    .vop_getpages   =   kokavfs_getpages,
    .vop_inactive   =   kokavfs_inactive,
    .vop_ioctl      =   kokavfs_ioctl,
    .vop_link       =   kokavfs_link,
    .vop_lookup     =   kokavfs_lookup,
    .vop_mkdir      =   kokavfs_mkdir,
    .vop_mknod      =   kokavfs_mknod,
    .vop_open       =   kokavfs_open,
    .vop_pathconf   =   kokavfs_pathconf,
    .vop_print      =   kokavfs_print,
    .vop_putpages   =   kokavfs_putpages,
    .vop_read       =   kokavfs_read,
    .vop_readdir    =   kokavfs_readdir,
    .vop_reclaim    =   kokavfs_reclaim,
    .vop_remove     =   kokavfs_remove,
    .vop_rename     =   kokavfs_rename,
    .vop_rmdir      =   kokavfs_rmdir,
    .vop_setattr    =   kokavfs_setattr,
    .vop_strategy   =   kokavfs_strategy,
    .vop_symlink    =   kokavfs_symlink,
    .vop_write      =   kokavfs_write,
};
