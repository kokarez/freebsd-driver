#include <sys/param.h>
#include "kokafs.h"

#include <sys/systm.h>
#include <sys/proc.h>
#include <sys/bio.h>
#include <sys/buf.h>
#include <sys/kernel.h>
#include <sys/sysctl.h>
#include <sys/vnode.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/malloc.h>
#include <sys/module.h>

static MALLOC_DEFINE(M_KOKAFSNODE, "kokafs_node", "KOKAFS vnode private part");

static int kokafs_cmount(struct mntarg *ma, void *data, uint64_t flags) {
  kokafs_trace();
  return 0;
}

static int kokafs_mount(struct mount *mp) {
  struct koka_mount *kokam;
  struct vnode      *vp;
  int               error;

  kokafs_debug("%s(mp = %p)\n", __func__, (void *)mp);
 
  kokam = malloc(sizeof (struct koka_mount), M_KOKAFSNODE, M_WAITOK);


  error = getnewvnode("koka", mp, &kokafs_vnodeops, &vp);
  if (error) {
    kokafs_debug("getnewvnode() = error (%d)\n", error);
    return error;
  }

  kokam->kokam_rootvp = vp; 
  mp->mnt_data = kokam;
  return 0;

}

static int kokafs_unmount(struct mount *mp, int mntflags) {
  kokafs_trace();
  return 0;
}

static int kokafs_root(struct mount *mt, int flags, struct vnode **vpp) {
  struct vnode *vp;

  kokafs_trace();
  vp = MOUNTTOKOKAMOUNT(mt)->kokam_rootvp;
  VREF(vp);

  vn_lock(vp, flags | LK_RETRY);
  *vpp = vp;
  return 0;
}

static int kokafs_statfs(struct mount *mt, struct statfs *sbp) {
  kokafs_trace();
  return 0;
}

static struct vfsops kokavfs_vfsops = {
  .vfs_cmount   = kokafs_cmount,
  .vfs_mount    = kokafs_mount,
  .vfs_unmount  = kokafs_unmount,
  .vfs_root     = kokafs_root,
  .vfs_statfs   = kokafs_statfs,
  /* Fill useless function by std* */
  .vfs_sync     = vfs_stdsync,
  .vfs_quotactl = vfs_stdquotactl,
  .vfs_init     = vfs_stdinit,
  .vfs_uninit   = vfs_stduninit
};

/**
 * See man page to understood this call
*/
VFS_SET(kokavfs_vfsops, kokafs, VFCF_NETWORK);
