#ifndef KOKAVFS_H
# define KOKAVFS_H

struct koka_mount {
  struct mount        *kokam_vfs;
  struct vnode        *kokam_rootvp;
  unsigned long long  kokam_flags;
};

# define MOUNTTOKOKAMOUNT(mp) ((struct koka_mount *)((mp)->mnt_data))

# define KOKATAG "[kokafs] "
# define kokafs_debug(format, args...) printf(KOKATAG format, ## args)
# define kokafs_trace() printf(KOKATAG "Trace: %s\n", __FUNCTION__)

extern struct vop_vector kokafs_vnodeops;

#endif /* !KOKAVFS_H */
