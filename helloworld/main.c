#include<sys/param.h>
#include<sys/module.h>
#include<sys/kernel.h>
#include<sys/systm.h>

static int event_handler(struct module *module, int event, void *arg) {
  int e = 0;

  switch (event) {
    case MOD_LOAD:
      uprintf("Driver has been correctly loaded\n");
      break;
    case MOD_UNLOAD:
      uprintf("Driver has been correctly unloaded\n");
      break;
    default:
      uprintf("Unknow event %d\n", event);
      break;
  }

  return e;
}

static moduledata_t module_conf = {
  "hello_koka",    /* module name */
  event_handler,   /* event handler */
  NULL             /* extra data */
};

DECLARE_MODULE(hello_koka, module_conf, SI_SUB_DRIVERS, SI_ORDER_MIDDLE);
